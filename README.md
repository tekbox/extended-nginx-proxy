Extends jwilder's nginx-proxy with some additional features.

For basic configuration you should look at [jwilder/nginx-proxy](https://github.com/jwilder/nginx-proxy)
 
## Usage of extended-nginx-proxy


## Configuration for extended-nginx-proxy

### Collect logs 

#### COLLECT_LOGS | false (default)

When `COLLECT_LOGS` is set to true, access logs for each container will collected in `/var/log/nginx-vhosts/`.


### Change private env key 
##### VIRTUAL_PRIVATE_ENV_KEY | VIRTUAL_PRIVATE (default)

To change the env key witch is used for private containers set `VIRTUAL_PRIVATE_ENV_KEY ` to a different value.

## Example

```yaml
nginx-proxy:
  build: extended-nginx-proxy
  container_name: nginx-proxy
  ports:
    - "127.0.0.1:80:80"
  volumes:
    - /var/run/docker.sock:/tmp/docker.sock:ro
    - ./volumes/conf.d/:/etc/nginx/conf.d/
    - ./volumes/vhost.d/:/etc/nginx/vhost.d
    - ./volumes/logs:/var/log/nginx-vhosts
    - ./volumes/htpasswd:/etc/nginx/htpasswd
  environment:
    - COLLECT_LOGS=true
    - VIRTUAL_PRIVATE_ENV_KEY=VIRTUAL_PRIVATE

```

## Configuration for client containters  

### Max Body Size

##### VIRTUAL_BIGBODY | false (default)

To increase the  `client_max_body_size` you could set `VIRTUAL_BIGBODY` to true. 
This will will set `client_max_body_size` to 1G. 

##### VIRTUAL_BIGBODY_SIZE | 1G (default)

To set the increased `client_max_body_size` to a diffrent value than 1G you could use `VIRTUAL_BIGBODY_SIZE`.

### Private 

##### VIRTUAL | false (default)
To restrict the access to a container to certain ip's, set `VIRTUAL_PRIVATE` to true. 

If `VIRTUAL_PRIVATE` is set to true `/etc/nginx/vhost.d/default_private` and `/etc/nginx/vhost.d/<hostname>_private` 
will be included if the exist.


## Example
```yaml
whoami:
    image: jwilder/whoami
    container_name: whoami
    environment:
      - VIRTUAL_BIGBODY=true
      - VIRTUAL_BIGBODY_SIZE=10G
      - VIRTUAL_PRIVATE=true
```

